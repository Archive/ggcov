/*
 * ggcov - A GTK frontend for exploring gcov coverage data
 * Copyright (c) 2001 Greg Banks <gnb@alphalink.com.au>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

#include "sourcewin.h"
#include "cov.h"
#include "estring.h"
#include "uix.h"

CVSID("$Id$");

extern GList *filenames;

static const char sourcewin_window_key[] = "sourcewin_key";
static gboolean sourcewin_initted = FALSE;

static void sourcewin_populate_filenames(sourcewin_t *sw);

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


#define SOURCE_COLUMNS	    (8+8+80) 	/* number,count,source */
#define SOURCE_ROWS	    (35)
#define MAGIC_MARGINX	    14
#define MAGIC_MARGINY	    5

sourcewin_t *
sourcewin_new(void)
{
    sourcewin_t *sw;
    GladeXML *xml;
    GtkTextBuffer *buffer;
    PangoFontDescription *font;
    
    sw = new(sourcewin_t);
    
    /* load the interface & connect signals */
    xml = ui_load_tree("source");
    
    sw->window = glade_xml_get_widget(xml, "source");
    ui_register_window(sw->window);
    sw->text = glade_xml_get_widget(xml, "source_text");
    sw->number_check = glade_xml_get_widget(xml, "source_number_check");
    sw->block_check = glade_xml_get_widget(xml, "source_block_check");
    sw->count_check = glade_xml_get_widget(xml, "source_count_check");
    sw->source_check = glade_xml_get_widget(xml, "source_source_check");
    sw->colors_check = glade_xml_get_widget(xml, "source_colors_check");
    sw->filenames_menu = ui_get_dummy_menu(xml, "source_file_dummy");
    sw->functions_menu = ui_get_dummy_menu(xml, "source_func_dummy");
    ui_register_windows_menu(ui_get_dummy_menu(xml, "source_windows_dummy"));

    font = pango_font_description_from_string ("monospace");
    gtk_widget_modify_font (sw->text, font);
    pango_font_description_free (font);

    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (sw->text));
    gtk_text_buffer_create_tag (buffer, "zero_count",
                                "foreground", "red",
                                NULL);
    
    gtk_object_set_data(GTK_OBJECT(sw->window), sourcewin_window_key, sw);

    gtk_window_set_default_size (GTK_WINDOW (sw->window),
                                 550, 500);
    
    gtk_widget_show(sw->window);
    
    sourcewin_populate_filenames(sw);

    return sw;
}

void
sourcewin_delete(sourcewin_t *sw)
{
    /* JIC of strange gui stuff */
    if (sw->deleting)
    	return;
    sw->deleting = TRUE;
    
    gtk_widget_destroy(sw->window);
    strdelete(sw->filename);

    g_free(sw);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static sourcewin_t *
sourcewin_from_widget(GtkWidget *w)
{
    w = ui_get_window(w);
    return (w == 0 ? 0 : gtk_object_get_data(GTK_OBJECT(w), sourcewin_window_key));
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void
on_source_filename_activate(GtkWidget *w, gpointer userdata)
{
    sourcewin_t *sw = sourcewin_from_widget(w);
    const char *filename = (const char *)userdata;
    
    sourcewin_set_filename(sw, filename);
}


static void
sourcewin_populate_filenames(sourcewin_t *sw)
{
    GList *iter;
    
    ui_delete_menu_items(sw->filenames_menu);
    
    for (iter = filenames ; iter != 0 ; iter = iter->next)
    {
    	const char *filename = (const char *)iter->data;

	ui_menu_add_simple_item(sw->filenames_menu, filename,
	    	    on_source_filename_activate, (gpointer)filename);
    }
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Sadly, this godawful hack is necessary to delay the
 * select/scroll code until after the Functions menu has
 * popped down, there being some sort of display bug
 * in GTK 1.2.7.
 */
typedef struct
{
    sourcewin_t *sourcewin;
    cov_function_t *function;
} sourcewin_hacky_rec_t;

static int
sourcewin_delayed_function_activate(gpointer userdata)
{
    sourcewin_hacky_rec_t *rec = (sourcewin_hacky_rec_t *)userdata;
    sourcewin_t *sw = rec->sourcewin;
    cov_function_t *fn = rec->function;
    const cov_location_t *first;
    const cov_location_t *last;

    first = cov_function_get_first_location(fn);
    last = cov_function_get_last_location(fn);
#if DEBUG
    fprintf(stderr, "Function %s -> %s:%ld to %s:%ld\n",
    	    	    	fn->name,
			first->filename, first->lineno,
			last->filename, last->lineno);
#endif
    
    /* Check for weirdness like functions spanning files */
    if (strcmp(first->filename, sw->filename))
    {
    	fprintf(stderr, "WTF?  Wrong filename for first loc: %s vs %s\n",
	    	    	first->filename, sw->filename);
	g_free(rec);
	return FALSE;
    }
    if (strcmp(last->filename, sw->filename))
    {
    	fprintf(stderr, "WTF?  Wrong filename for last loc: %s vs %s\n",
	    	    	last->filename, sw->filename);
	g_free(rec);
	return FALSE;
    }

    sourcewin_ensure_visible(sw, first->lineno);

    /* This only selects the span of the lines which contain executable code */		    
    sourcewin_select_region(sw, first->lineno, last->lineno);

    g_free(rec);
    return FALSE;   /* and don't call me again! */
}

static void
on_source_function_activate(GtkWidget *w, gpointer userdata)
{
    sourcewin_hacky_rec_t *rec;
    
    rec = new(sourcewin_hacky_rec_t);
    rec->sourcewin = sourcewin_from_widget(w);
    rec->function = (cov_function_t *)userdata;

    gtk_idle_add(sourcewin_delayed_function_activate, rec);
}

static int
compare_functions(const void *a, const void *b)
{
    const cov_function_t *fa = (const cov_function_t *)a;
    const cov_function_t *fb = (const cov_function_t *)b;
    
    return strcmp(fa->name, fb->name);
}

static void
sourcewin_populate_functions(sourcewin_t *sw)
{
    GList *functions = 0;
    unsigned fnidx;
    cov_file_t *f;
    cov_function_t *fn;
    
    /* build an alphabetically sorted list of functions */
    f = cov_file_find(sw->filename);
    assert(f != 0);
    for (fnidx = 0 ; fnidx < f->functions->len ; fnidx++)
    {
    	fn = f->functions->pdata[fnidx];
	
	if (!strncmp(fn->name, "_GLOBAL_", 8))
	    continue;
	functions = g_list_prepend(functions, fn);
    }
    
    functions = g_list_sort(functions, compare_functions);
    
    /* now build the menu */

    ui_delete_menu_items(sw->functions_menu);
    
    while (functions != 0)
    {
    	fn = (cov_function_t *)functions->data;
	
	ui_menu_add_simple_item(sw->functions_menu, fn->name,
	    	    on_source_function_activate, fn);
	
	functions = g_list_remove_link(functions, functions);
    }
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

/*
 * Expand tabs in the line because GTK 1.2.7 through 1.2.9 can
 * only be trusted to expand *initial* tabs correctly, DAMMIT.
 */
static char *
fgets_tabexpand(char *buf, unsigned int maxlen, FILE *fp)
{
    int c;
    unsigned int len = 0, ns;
    static const char spaces[] = "        ";
    
    maxlen--;	/* allow for nul terminator */
    
    for (;;)
    {
    	if ((c = getc(fp)) == EOF)
	    return (len == 0 ? 0 : buf);
	    
	if (c == '\t')
	{
	    ns = 8-(len&7);
	    if (len+ns >= maxlen)
	    {
	    	ungetc(c, fp);
		return buf;
	    }
	    memcpy(buf+len, spaces, ns);
	    len += ns;
	    buf[len] = '\0';
	}
	else
	{
	    if (len+1 == maxlen)
	    {
	    	ungetc(c, fp);
		return buf;
	    }
	    buf[len++] = c;
	    buf[len] = '\0';
	    if (c == '\n')
	    	break;
	}
    }
    
    return buf;
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

static void
format_blocks(char *buf, unsigned int width, const cov_location_t *loc)
{
    const GList *blocks;
    unsigned int len;
    
    for (blocks = cov_blocks_find_by_location(loc) ; 
    	 blocks != 0 && width > 0 ;
	 blocks = blocks->next)
    {
    	cov_block_t *b = (cov_block_t *)blocks->data;
    	len = snprintf(buf, width, "%u%s", b->idx, (blocks->next == 0 ? "" : ","));
	buf += len;
	width -= len;
    }
    
    for ( ; width > 0 ; width--)
    	*buf++ = ' ';
    *buf = '\0';
}

static void
sourcewin_update_source(sourcewin_t *sw)
{
    FILE *fp;
    cov_location_t loc;
    count_t count;
    gboolean have_count;
    GtkTextView *text = GTK_TEXT_VIEW(sw->text);
    int i, nstrs;
    const char *strs[5];
    char linenobuf[32];
    char blockbuf[32];
    char countbuf[32];
    char linebuf[1024];
    GtkTextBuffer *buffer;
    gboolean zero_count_tag;
    
    if ((fp = fopen(sw->filename, "r")) == 0)
    {
    	/* TODO: gui error report */
    	perror(sw->filename);
	return;
    }

    buffer = gtk_text_view_get_buffer (text);

    gtk_text_buffer_set_text (buffer, "", -1);

    loc.filename = sw->filename;
    loc.lineno = 0;
    while (fgets_tabexpand(linebuf, sizeof(linebuf), fp) != 0)
    {
    	++loc.lineno;
	
	cov_get_count_by_location(&loc, &count, &have_count);

    	/* choose colours */
        zero_count_tag = FALSE;
	if (GTK_CHECK_MENU_ITEM(sw->colors_check)->active && have_count)
            zero_count_tag = count == 0;

    	/* generate strings */
	
	nstrs = 0;
	
	if (GTK_CHECK_MENU_ITEM(sw->number_check)->active)
	{
	    snprintf(linenobuf, sizeof(linenobuf), "%7lu ", loc.lineno);
	    strs[nstrs++] = linenobuf;
	}
	
	if (GTK_CHECK_MENU_ITEM(sw->block_check)->active)
	{
	    format_blocks(blockbuf, 16, &loc);
	    strs[nstrs++] = blockbuf;
	}
	
	if (GTK_CHECK_MENU_ITEM(sw->count_check)->active)
	{
	    if (have_count)
	    {
		if (count)
		{
		    snprintf(countbuf, sizeof(countbuf), "%7lu ", (unsigned long)count);
		    strs[nstrs++] = countbuf;
		}
		else
		    strs[nstrs++] = " ###### ";
	    }
	    else
		strs[nstrs++] = "        ";
    	}

	if (GTK_CHECK_MENU_ITEM(sw->source_check)->active)
	    strs[nstrs++] = linebuf;
	else
	    strs[nstrs++] = "\n";

    	for (i = 0 ; i < nstrs ; i++)
	{
            GtkTextIter end;

            gtk_text_buffer_get_end_iter (buffer, &end);
            
    	    gtk_text_buffer_insert_with_tags_by_name (buffer,
                                                      &end,
                                                      strs[i], -1,
                                                      zero_count_tag ?
                                                      "zero_count" : NULL,
                                                      NULL);
        }
    }
    
    sw->max_lineno = loc.lineno;
    
    fclose(fp);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void
sourcewin_set_filename(sourcewin_t *sw, const char *filename)
{
    ui_window_set_title(sw->window, filename);
    strassign(sw->filename, filename);
    sourcewin_update_source(sw);
    sourcewin_populate_functions(sw);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void
sourcewin_select_region(
    sourcewin_t *sw,
    unsigned long startline,
    unsigned long endline)
{
    GtkTextIter start, end;
    GtkTextBuffer *buffer;

    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (sw->text));
    
    gtk_text_buffer_get_iter_at_line (buffer, &start, startline);
    gtk_text_buffer_get_iter_at_line (buffer, &end, endline);

    gtk_text_buffer_select_range (buffer, &start, &end);
}

void
sourcewin_ensure_visible(sourcewin_t *sw, unsigned long line)
{
    GtkTextIter iter;
    GtkTextBuffer *buffer;

    buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (sw->text));
    
    gtk_text_buffer_get_iter_at_line (buffer, &iter, line);

    gtk_text_view_scroll_to_iter (GTK_TEXT_VIEW (sw->text),
                                  &iter,
                                  0.0, FALSE, 0.0, 0.0);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GLADE_CALLBACK void
on_source_close_activate(GtkWidget *w, gpointer data)
{
    sourcewin_t *sw = sourcewin_from_widget(w);
    
    if (sw != 0)
	sourcewin_delete(sw);
}

GLADE_CALLBACK void
on_source_exit_activate(GtkWidget *w, gpointer data)
{
    gtk_main_quit();
}

GLADE_CALLBACK void
on_source_count_check_activate(GtkWidget *w, gpointer data)
{
    sourcewin_t *sw = sourcewin_from_widget(w);
    
    sourcewin_update_source(sw);
}

GLADE_CALLBACK void
on_source_number_check_activate(GtkWidget *w, gpointer data)
{
    sourcewin_t *sw = sourcewin_from_widget(w);
    
    sourcewin_update_source(sw);
}

GLADE_CALLBACK void
on_source_block_check_activate(GtkWidget *w, gpointer data)
{
    sourcewin_t *sw = sourcewin_from_widget(w);
    
    sourcewin_update_source(sw);
}

GLADE_CALLBACK void
on_source_source_check_activate(GtkWidget *w, gpointer data)
{
    sourcewin_t *sw = sourcewin_from_widget(w);
    
    sourcewin_update_source(sw);
}

GLADE_CALLBACK void
on_source_colors_check_activate(GtkWidget *w, gpointer data)
{
    sourcewin_t *sw = sourcewin_from_widget(w);
    
    sourcewin_update_source(sw);
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/*END*/
